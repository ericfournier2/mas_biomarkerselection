for i in output/methylation_extractor/Run*/*_bismark_bt2_pe.bismark.cov.gz
do 
    zcat $i | grep -e "^[0-9XYM]" | gzip -c > $i.filtered.gz
done
