#!/bin/bash
module load mugqic/bismark
module load samtools
module load bowtie2

bismark_genome_preparation --verbose output/bismark_genome